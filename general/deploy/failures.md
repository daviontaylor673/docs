# Deploy Failures

Sometimes a deploy pipeline fails.  We should always be aware of these failures
through notifications coming in through slack on `#announcements`, take a look
at the pipeline, and take the appropriate actions.

## Failure Cases

* QA - this would happen after a deployment has occurred successfully and now QA
  jobs will execute.  If any of the QA tasks fail, the Quality department is
  notified.  The Release Manager should follow up on these failures to determine
  if the failure is a blocker that should prevent to initiate investigation. The
  deploy from proceeding any further, and that an issue exists in the [gitlab
  issue tracker] to issue a fix of the problem. This should be labeled as high
  priority as if it is determined a regression is found, we do not want to move
  the deployment forward and need to initiate the appropriate Engineering teams
  quickly.
  * Reach out to Quality in `#quality` and contact the appropriate team members:
    https://about.gitlab.com/handbook/engineering/quality/#department-members
  * Quality also has helpful guides published here:
    * https://about.gitlab.com/handbook/engineering/quality/guidelines/#test-failures
    * https://about.gitlab.com/handbook/engineering/quality/guidelines/debugging-qa-test-failures/
* Prep Job - The prepare job perform a few items of work, one related to
  ensuring that servers are in the appropriate state in our load balancers and
  that the version of GitLab is the same across the fleet.  Action must be taken
  manually in order to remediate this type of failure.
* Fleet Deploy - This can fail for a myriad of reasons.  Each failure should be
  investigated to determine the appropriate course of action accompanied with an
  issue where appropriate.  Common fixes are noted below.
* Post Deploy Migrations - normally encountered when there's a statement
  timeout.  Halt any further action on the deploy.  Try and determine which
  migration is at fault by looking for the targeted migration in the job output.
  Find the associated issue or Merge Request associated with that migration and
  engage development immediately.  Normal course  of action would be to revert,
  but reliance on Development teams can confirm if this is an option.

## Troubleshooting

### Production Deploy Failed, but okay to leave in place

In situations where production deploys fail for any reason (such as post deploy
migration failures), but it is deemed safe to leave production as is, we need to
ensure that we don't prevent future deploys from being blocked.  Run the
following from the `deploy-tooling` repository:

```
CURRENT_DEPLOY_ENVIRONMENT=<env> DEPLOY_VERSION=<version> ./bin/set-version
./bin/set-omnibus-updates gstg --enable
```

This will configure the `<env>-omnibus-version` chef role to the appropriate
version and ensure installation is enabled of that version.  This happens
automatically during successful pipelines.

This is slated to become a chatops run set of commands: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/524

### Prep job discovered nodes in `DRAIN`

The prep job (`<ENV>-prepare`) will fail if nodes are not in state `UP` or `MAINT`. Any other
state and the prep job will hard fail noting which frontend server contains the
state, and which backend server is in this state. Unless there's known
maintenance happening there should not exist a situation where a server isn't in
either of the preferred states.

If a server is in state `DRAIN` a prior deploy may not have fully completed.
In this case, set the server into `MAINT` and retry.

This can be accomplished by following the [documentation for setting haproxy server state](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/haproxy.md#set-server-state).
If you don't have access to the nodes, ask the SRE on call for GitLab Production to help you with this. You can find the engineer on call through ChatOps:

```bash
/chatops run oncall production
```

### Fleet Deploy

* `E: Could not get lock /var/lib/apt/lists/lock - open (11: Resource
  temporarily unavailable)`
  * This is common if there's a collision with chef running and the deploy
    trying to perform similar actions
  * Though we've tried to eliminate these issues as much as possible, hitting
    retry is usually the best method to allow the deploy to continue
* Timeout when setting haproxy state
  * This is common for our `pages` fleet when pages is starting up
  * [Pages service takes a long time to start up], hitting retry for this job
    helps.  The timeout is already very high on this particular task, if we hit
    the timeout again, we should open an issue to investigate further.
  * If this happens on a server unrelated to the `pages` service starting, this
    must be deeply investigated on the node that exhibited the failure

[gitlab issue tracker]: https://gitlab.com/gitlab-org/gitlab/issues
[Pages service takes a long time to start up]: https://gitlab.com/gitlab-org/gitlab-pages/issues/41
