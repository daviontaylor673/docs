# Monthly Release

The [Release Manager] should always be working on the release.  Most of the work
is simply monitoring the auto-deploy process and watching the appropriate
channels of communication for the next steps.  Always be cognizant that we may
need to stop the auto-deploy process if a P1 issue is announced by an
Engineer performing testing.

## Process

### Create an issue to track the release

In order to keep track of the various tasks that need to happen each day leading
up to the final release, we create an issue on the [release task tracker] and
update it as we progress.

1. Using Slack in `#releases` execute: `/chatops run release prepare <VERSION>`
    * Example: `/chatops run release prepare 11.8.0`
1. ChatOps will respond with a job that gets executed as well as a link to the
   various issues that are created automatically.

This meta issue will serve as the main place where everyone can find issues
related to the release you will be working on.

Every time you create a new issue for one of the upcoming tasks, you should
link it to this meta issue.

### Auto-Deploy Process

Auto-Deploy will create a package each day if necessary.  It'll be announced in
the slack `#announcements` channel when that package has been deployed to a
particular environment.  It's the
job of the Release Manager to monitor the auto-deploy branch and validate it has
not succumb to any Pipeline failure.  We should also ensure that any deploy to
any environment is also both successful and error free.   We strive to deploy to
production each day but only after it's been deemed safe to do so.

The Auto-deploy process is documented in much greater detail here:
[gitlab.com/gitlab-org/release/docs/.../general/deploy/auto-deploy.md](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy.md)

#### Guidelines on Deploy

Deploys to Staging and Canary are automatic after the completion of a successful
package build.  Deploying to each of these also runs a set of full QA jobs.
Canary is gated by the QA job from the Staging deploy.  See [QA
Failures](../deploy/failures.md#failure-cases) to address situations where QA
jobs fail.

A QA issue is opened upon successful deploy to the staging environment. When a
QA issue is opened, a listing of issues that made it into said package will be
available for Engineers to check off and validate features are working as
intended.  Use this only as a guideline to determine if it is safe to promote
that package to production.  It's also wise to look at the issues in the QA
tasks to see if anything looks glaring that would cause one to ask questions to
the associated Engineer.  Note that we no longer utilize the QA issue as a
blocker to prevent us from promoting to Production.

As auto-deploy moves along, the beginning of the week will always have the most
changes, and taper off through the week.  This creates a few notable situations
to make decisions upon.

QA tasks may not be created.  This can be one of three things:

1. Nothing was picked into the auto-deploy branch that round, which means no
   GitLab package was created which resulted in no deploy.
1. Another situation may be a sign of a problem during the deploy to the staging
   environment.  This should be investigated.
1. [Some items are excluded from showing up in
   QA](https://gitlab.com/gitlab-org/release-tools/blob/e3bdef3ecef01937a6c3a9b3f6c8c7f12cf0e488/lib/release_tools/qa.rb).
   The version of the product will still change but functionality of GitLab may
   not.  In these circumstances, it's safe to skip deploying to Production.

We should strive to deploy to production as often as possible.  We cannot expect
every item on the QA checklist to be checked off.  In those instances, we must
look at the list and see if anything is questionable.  Take a look at our
metrics for the canary environment, look at Sentry for errors.  If nothing is
raising any flags it may be safe to proceed with a deploy.

Promotion to production is a manual step.  As a guideline **wait for 1 hour**
prior to beginning the process.  Prior to asking for permission, observe the
following:

1. No new errors in Sentry for that release
    * Find the current release running on canary, and go to the Releases page in
      Sentry and see if there's any new errors to be concerned about
1. Ensure that QA jobs in staging and Canary are successful.

If all appears okay, proceed to ask for permission to promote Canary to
Production.  Utilize the #production channel in slack, and ping `@sre-oncall`.
Upon approval from the on-call, proceed to promote.

Be wary of times where we hold a deploy into production.  The next day we run
into a situation where new code that made it to canary never made it to
production yet which puts us in a questionable situation as we'll have more
changes that originally intended to land in the next production deploy.  We
should try to avoid pushing an older version of GitLab into production due to
code changes potentially behaving badly due to data migrations.

Use your best judgement to determine if production should move forward.  Ask
questions to ensure you are comfortable and overly communicate your decision to
or not to move forward.  In general we want to avoid creating situations where
production is behind canary for lengthy periods of time.

Our documentation for how our deployer mechanism works can be found here:
[gitlab.com/gitlab-org/release/docs/.../general/deploy/gitlab-com-deployer.md#creating-a-new-deployment-for-upgrading-gitlab](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/gitlab-com-deployer.md#creating-a-new-deployment-for-upgrading-gitlab)

### Complete the release tasks

Once the release schedule begins, each work day has something that needs to be
done. Perform the tasks and mark them as complete in the issue as you progress.

If you're not sure what to do for any task, [check the guides](../README.md#guides).

## Getting Help

Completing release tasks on time is very important. If you experience problems with any of
release tasks and you don't know who to ask then you should contact someone from this list:

* Build Lead [@marin](https://gitlab.com/marin)
* VP of Engineering [@edjdev](https://gitlab.com/edjdev)

The earlier we determine problem or delay in release - the easier it is to fix it.

## Priorities

Keep up with the release schedule. It's better to ship less but on time.
Revert code that delays the release.

---

[Return to Guides](../README.md#guides)

[on the blog]: https://about.gitlab.com/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab/
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/issues
[Release Manager]: ../../quickstart/release-manager.md
