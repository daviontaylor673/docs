# General process for Security engineer in security releases

The main difference is in who initiates the release process. 
* **[Critical](#critical-security-releases)** security releases are initiated on-demand by the security team to resolve the most severe vulnerabilities that are labelled as `S1`. They are highly urgent and need to be released as soon as the patch is ready and it is possible to perform a release.
* **[Regular](#regular-security-releases)** security releases are scheduled monthly and are initiated by the release management team. 

## Dependencies updates
Fixes that mitigate 3rd party vulnerabilities are typically added at the end of the blog post with a reference to the existing CVE ID that was assigned to that vulnerability by the 3rd party.

[Example of previously used wording](https://about.gitlab.com/releases/2020/03/26/security-release-12-dot-9-dot-1-released/):
```
### Update Nokogiri dependency
The Nokogiri dependency has been upgraded to 1.10.8. This upgrade include a security fix for [CVE-2020-7595](https://github.com/advisories/GHSA-7553-jr98-vx47).
```



## Overall process

1. [Prepare blog post](#prepare-blog-post)
2. Follow the appropriate sub-process for a [critical](#critical-security-releases) or [regular](#regular-security-releases) security release
3. [Verify fixes](#verify-fixes)
4. [Finalize the release](#finalize-release)
6. [Post release](post-release). Once completed, you have finished the security release.

### Prepare blog post

* :warning: The developer has provided the range of affected versions: ask on the related security issues to provide it if not already provided.
    * This is required to request [CVEs](https://cveform.mitre.org/).
* Create a WIP blog post MR in dev.gitlab.org:
  1. Ensure to add the CVE IDs, vulnerability descriptions, affected versions, remediation info, and a thank you to the reporter. 
       * Much of this information can be copy and pasted from the [summary table](https://dev.gitlab.org/gitlab/gitlabhq/-/blob/master/.gitlab/issue_templates/Security%20Developer%20Workflow.md#summary) in the developer security issue created on [GitLab Security]. 
  1. Feel free to use a past security release as a guide, for example [this blog post](https://about.gitlab.com/releases/2020/05/27/security-release-13-0-1-released/).
* Please add `/source/images/blogimages/security-cover-new.png` to the `image_title:` field in the front matter.
* Please include a "Receive Security Release Notifications" section at the very bottom of the blog post with links to our contact us page and RSS feed. See previous security release posts or [this issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues/145#note_240450797) for examples.
* Assign the blog post to another member of the appsec team for review.
* Create a WIP issue using the `Security Release Email Alert` template in https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues for the communications team and request an email notification be sent to subscribers of the `Security Alert` segment on the day of the release. Include a note that this should be sent out **after** the blog post is live. Also mention that you'll include the link to the blog post MR once it is prepared. The content of this notification should be similar to the blog post introduction:

>"Today we are releasing versions X.X.X, X.X.X, and X.X.X for GitLab Community Edition (CE) and Enterprise Edition (EE).

>These versions contain a number of important security fixes, and we strongly recommend that all GitLab installations be upgraded to one of these versions immediately. 

>Please forward this alert to appropriate people at your organization and have them subscribe to [Security Notices](https://about.gitlab.com/contact/). You can also receive security release blog updates by subscribing to our [RSS feed](https://about.gitlab.com/security-releases.xml).

>For more details about this release, please visit our [blog](TODO)."

### Regular security releases


1. For each provided fix for a security issue:
    1. Request a pre-assigned CVE using the [webform][cve].
1. Set the previous month's regular security release to public by following the [post release steps](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/security-engineer.md#post-release) for it.
1. Create a new issue for the next scheduled security release and begin adding issues which are ready to be backported and released.  This may need to happen earlier to track fixes that did not meet the deadline. Document this new release issue in the title of the #releases Slack channel.

### Critical security releases

The issue must be prepared as soon as we are aware of the critical issue.

Once a fix has been provided for a critical `S1` labelled security issue:

1. Request a pre-assigned CVE using the [webform][cve].
1. Notify release managers of a need to prepare for the critical security
   release. If the fix is not ready yet, provide release managers with an
   estimate of when the fix is going to be ready so that they can create a
   release plan.
1. **30 days after** the release is published, follow steps in [Post Release](#post-release).

#### Verify fixes

* :warning: The code is merged on the security mirror `https://gitlab.com/gitlab-org/security/gitlab`: make sure the code is merged as soon as possible.
* Validate the fixes. Testing can be performed on either:
  * The `staging.gitlab.com` environment.
  * Locally via the docker images of the built packages at https://dev.gitlab.org/gitlab/omnibus-gitlab/container_registry
    * i.e. `docker pull dev.gitlab.org:5005/gitlab/omnibus-gitlab/gitlab-ee:X.X.X-ee.0`
  * using the gdk:
```
gdk init [directory]
gdk install gitlab_repo=https://gitlab.com/gitlab-org/security/gitlab

# Switch to the related fix branch, and then start the GDK:
gdk start
# stop the GDK prior switching to another branch
```

* Verify fixes on all backports on environments provided by Quality.
* Once validated, assign the issue back to the release manager(s) and notify them that the fixes are ready to be published.

#### Finalize release

Once blog post on dev.gitlab.org has been reviewed and all packages are ready for
publishing:

* Release manager starts publishing the packages
* **When the packages are ready to be merged by the Release Team into master branch:** create an MR with the blog post file in https://gitlab.com/gitlab-com/www-gitlab-com.
  * Destination Directory: `source/releases/posts` by copying the blog post MR that was prepared on `dev.gitlab.org`. 
  * The file name should be in the format **`YYYY-MM-DD-security-release-gitlab-X.X.X.released.html.md`**
* Put the link of the new blog post in the email notification request issue as well as the security release issue
* Make sure the CVE IDs are documented in the corresponding GitLab.com issues and H1 reports
* Ping in H1 reports the appsec team member who triaged the issue to notify a fix has been released
* Close out the issues on both `gitlab-org/gitlab` and `gitlab-org/security/gitlab`.

#### Post release

30 days after release
 * Open a new security release issue for the next security release. Identify the next release managers from the [release managers page](https://about.gitlab.com/release-managers/) and assign the issue to them.
 * Remove `confidential` from issues unless they have been labelled `~keep confidential`.
   * Our **bot** will automatically remind us to remove `confidential` from the issues.
 * Update the CVE IDs to public. See `Notify CVE about a publication` on the [webform](https://cveform.mitre.org/).

[cve]: https://cveform.mitre.org/
[GitLab Security]: https://gitlab.com/gitlab-org/security/gitlab
