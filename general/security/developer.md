# Security Releases (Critical / Non-critical) as a Developer

The release deadlines for a critical or non-critical security release are different.
Check the [Release deadlines](process.md) first to know when the security
fixes have to be merged by.

## DO NOT PUSH TO `gitlab-org/gitlab`

As a developer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it.

To that end, you'll need to be sure that security vulnerabilities:

* For GitLab.com, are fixed in the [GitLab Security Repo].
* For Omnibus, are fixed in the [Omnibus Security Repo].
* For GitLab Pages, are fixed in the [GitLab Pages Security Repo].

This is fundamental to our security release process because both repositories are not publicly-accessible.

## Process

As with most GitLab development, a security fix starts with an issue identifying
the vulnerability. In this case, it should be a confidential issue on the `gitlab-org/gitlab`
project on [GitLab.com]

Once a security issue is assigned to a developer, we follow the same merge
request and code review process as any other change, but on [GitLab Security Repo].
All security fixes are released for [at least three monthly releases], and you
will be responsible for creating backports as well.

When working on a high severity ~S1 issue, the initial work on a [post-deployment patch](utilities/post_deployment_patch.md), may be done before any
other tasks in the normal workflow at the request of the security engineer, with
the review of the delivery team and infrastructure team as outlined in the
post-deployment patch process.

### Preparation

Before starting, add the new `security` remote on your local GitLab repository:

```
git remote add security git@gitlab.com:gitlab-org/security/gitlab.git
```

From %12.6, the development of security fixes was moved to [a private group on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/121),
make sure you remove any `dev.gitlab.org` remotes from your local repository:

```
git remote -v | grep dev.gitlab.org | awk '{print $1}' | uniq | xargs git remote rm
```

Finally, run the `scripts/security-harness` script. This script will install a Git `pre-push` hook
that will prevent pushing to any remote besides `gitlab.com/gitlab-org/security` or `dev.gitlab.org`,
in order to prevent accidental disclosure.

Please make sure the output of running `scripts/security-harness` is:

```
Security harness installed -- you will only be able to push to dev.gitlab.org or gitlab.com/gitlab-org/security
```

### Branches

Because all security fixes go into [at least three monthly releases], four branches will need to be created
for your security fix:

* One targeting `master`.
* Another 3 targeting the last 3 monthly releases.
  * This includes the current release, so for a security fix that's implemented in the 12.8 cycle,
    the backports are for 12.8, 12.7, and 12.6.

Detailed instructions for the creation of these branches can be found on the
[Security developer workflow](https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md)
issue.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-10-6`.

Branch names that start with `security` will be rejected upon a push from the [GitLab] and [GitLab-FOSS]
repositories on GitLab.com. This helps ensure security merge requests don't get
leaked prematurely.

### Development

#### Create an issue

Create an issue on the respective repo, [GitLab Security Repo] or [Omnibus Security Repo], using the
[Security Developer Workflow]. The title should be the same as the original created on [GitLab.com],
for example: `Prevent stored XSS in code blocks`.

It's not required to copy all the labels from the original issue on `gitlab-org/gitlab`, ~security is enough. 

This issue is now your "Implementation issue" and a single source of truth for
all related issues and merge requests. Once the issue is created, assign it
to yourself and start working on the tasks listed there.

##### Versions Affected

Within the description "Details" section, fill out the 
[Versions affected](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md#L58) 
using one of the following formats:

* `All`
* `X.Y+`
* `X.Y - X.Y`
* `X.Y,X.Y`

#### Create merge requests

The above issue will ask you to create merge requests on [GitLab Security Repo] or [Omnibus Security Repo].
Ensure they are using [Security Release template].

For every merge request created, make sure:

* Targets the `X-Y-stable{,-ee}` branch that belongs to the target version.
  * For the MR targeting the current release, the stable branch might not yet exist.
    They are normally created around the 18th. In the meantime, you can set the MR to target `master`
    instead, and change it later once the stable branch has been created.
* Has a milestone assigned, e.g. a `11-10-stable-` backport MR would have `11.10` set as its milestone.
  * The MR targeting `master` has the next upcoming milestone assigned.
  * Milestones that have already been closed are not displayed in the UI, but can still be set using the quick action command `/milestone %X.Y`.
* Has correct labels (normally ~security is enough).
* Has green pipelines.

**IMPORTANT:** 
* In case one of these items is missing or incorrect, Release Managers will re-assign
all related merge requests to the original author and remove the issue from the current security release.
This is necessary due to the number of merge requests that need to be handled for each security release. 
* Merge requests targeting master are to be merged by Release Managers only.

When creating your merge requests backports there's a handy script, [`secpick`](utilities/secpick_script.md),
that will allow you to cherry-pick commits across multiple releases. If changes
can't be cleanly picked (e.g. file changed doesn't exist or file was moved in the
previous version), you will need to fix it manually.

**TIP:**
When implementing a security fix, it's best to go with the smallest change possible.
This is helpful to avoid problems/conflicts when creating backports for older
versions. It also helps to reduce the possibility of having unwanted side effects
as the fix will be focused on the issue. Improvements can be done publicly after
the security release is done.

#### Code reviews

Security merge requests follow the same review process stated in our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html):

* Merge requests targeting `master` should be approved according to our [Approval Guidelines](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines)
* Backport merge requests should be approved by at least one maintainer. Maintainer must be the same as the one assigned to the merge request targeting `master`.

### Final steps

* Ensure all items have been completed:
   * Including [Documentation and final details] from the security implementation issue.
   * Items listed on the [Security Release template] for each merge request.
* Ensure all merge requests associated to the security implementation issue are assigned to `@gitlab-release-tools-bot`,
ping the corresponding maintainer if that's not the case.
* Be sure to run `scripts/security-harness` again to enable pushing to remotes other than [GitLab Security].

### Questions?

If you have any doubts or questions, feel free to ask for help in the #releases or #g_delivery
channel in Slack.

---

[Return to Security Guide](process.md)

[at least three monthly releases]: https://docs.gitlab.com/ee/policy/maintenance.html#security-releases
[GitLab.com]: https://gitlab.com/
[GitLab Security]: https://gitlab.com/gitlab-org/security/ 
[GitLab Security repo]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus Security repo]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[Security Developer Workflow]: https://gitlab.com/gitlab-org/security/gitlab/issues/new?issuable_template=Security+developer+workflow
[scripts/security-harness]: https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/security-harness
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[GitLab-FOSS]: https://gitlab.com/gitlab-org/gitlab-foss
[Security Release template]: https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/merge_request_templates/Security%20Release.md
[GitLab Pages Security Repo]: https://gitlab.com/gitlab-org/security/gitlab-pages
[Documentation and final details]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md#documentation-and-final-details
